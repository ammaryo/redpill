﻿using System;
using NUnit.Framework;
using KnockKnock;


namespace KnockKnock.Test
{
    [TestFixture]
    public class WhatShapeIsThisTests
    {
        [TestCase(0,3,4)]
        [TestCase(3, 0, 4)]
        [TestCase(4, 3, 0)]
        public void OneSideIsZero_Should_Return_Error(int a,int b,int c)
        {
            RedPill redpill = new RedPill();
            var expected = TriangleType.Error;

            var actual = redpill.WhatShapeIsThis(a, b, c);
            Assert.AreEqual(expected, actual);


        }

        [TestCase(-1, 3, 3)]
        [TestCase(3, -1, 3)]
        [TestCase(3, 3, -1)]
        [TestCase(int.MinValue,int.MinValue,int.MinValue)]
        public void OneSideIsLessThanZero_Should_Return_Error(int a, int b, int c)
        {
            RedPill redpill = new RedPill();
            var expected = TriangleType.Error;

            var actual = redpill.WhatShapeIsThis(a, b, c);
            Assert.AreEqual(expected, actual);

        }

        [TestCase(10,3,4)]
        [TestCase(4, 3, 10)]
        [TestCase(3, 4, 10)]
        public void OneSideIsLargerThanSumOfOtherTwoSides_Should_Return_Error(int a,int b,int c)
        {
            RedPill redpill = new RedPill();
            var expected = TriangleType.Error;

            var actual = redpill.WhatShapeIsThis(a, b, c);
            Assert.AreEqual(expected, actual);

        }
        [TestCase(2,1,2)]
        [TestCase(2, 2, 1)]
        [TestCase(1, 2, 2)]
        public void TwoSidesOnly_Should_Return_Isosceles(int a,int b,int c)
        {
            RedPill redpill = new RedPill();
            var expected = TriangleType.Isosceles;

            var actual = redpill.WhatShapeIsThis(a, b, c);
            Assert.AreEqual(expected, actual);

        }
        [TestCase(4,3,5)]
        [TestCase(5, 4, 3)]
        [TestCase(3, 4, 5)]
        public void NoSidesAreEqual_Should_Return_Scalene(int a,int b,int c)
        {
            RedPill redpill = new RedPill();
            var expected = TriangleType.Scalene;

            var actual = redpill.WhatShapeIsThis(a, b, c);
            Assert.AreEqual(expected, actual);


        }
        [TestCase(2,2,2)]
        [TestCase(int.MaxValue,int.MaxValue,int.MaxValue)]
        public void AllSides_Should_Return_Equilateral(int a,int b,int c)
        {
            RedPill redpill = new RedPill();
            var expected = TriangleType.Equilateral;

            var actual = redpill.WhatShapeIsThis(a, b, c);
            Assert.AreEqual(expected, actual);

        }
       
      
        
    }
}
