﻿using System;
using NUnit.Framework;

namespace KnockKnock.Test
{
    [TestFixture]
    public class WhatShapeFibonacciNumberTests
    {
        [Timeout(50)]
        [TestCase(93),TestCase(-93)]
        public void N_GreaterThan_92_Should_Throw_Excption(long n)
        {
            RedPill redpill = new RedPill();

            Assert.Throws<OverflowException>(delegate { 
                redpill.FibonacciNumber(n);
            });

            
        }

        
        [Timeout(50)]
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        [TestCase(3, 2)]
        [TestCase(4,3)]
        [TestCase(5, 5)]
        [TestCase(6, 8)]

        [TestCase(-1, 1)]
        [TestCase(-2, -1)]
        [TestCase(-3, 2)]
        [TestCase(-4, -3)]
        [TestCase(-5, 5)]
        
        
        [TestCase(92, 7540113804746346429)]
        [TestCase(-92, -7540113804746346429)]

        [TestCase(91, 4660046610375530309)]
        [TestCase(-91, 4660046610375530309)]
        public void FibTest(long n,long expected)
        {
            RedPill redpill = new RedPill();

            var actual = redpill.FibonacciNumber(n);
            Assert.AreEqual(expected, actual);
        }
       
        


       
        
    }
}
