﻿using NUnit.Framework;
using System;


namespace KnockKnock.Test
{
    [TestFixture]
    public class ReverseWordsTests
    {
        [Test]
        public void cat_and_dog_shouldbe_tac_dna_god()
        {
            RedPill redpill = new RedPill();
            var expected = "tac dna god";
            var actual = redpill.ReverseWords("cat and dog");

            Assert.AreEqual(expected, actual);
        }

       
       
       
        


        [TestCase("~!@#$%^&*()_+", "+_)(*&^%$#@!~")] //special chars

        [TestCase("yay k!k b@b", "yay k!k b@b")] //special chars plaindrome

        [TestCase(" cat and dog", " tac dna god")] //single
        [TestCase("  cat and dog", "  tac dna god")] //double
        [TestCase("   cat and dog", "   tac dna god")] //triple

        [TestCase(" cat and dog ", " tac dna god ")] //single
        [TestCase("  cat  and  dog  ", "  tac  dna  god  ")] //double
        [TestCase("   cat   and   dog   ", "   tac   dna   god   ")] //triple

        [TestCase("cat and dog ","tac dna god ")] //single
        [TestCase("cat and dog  ","tac dna god  ")] //double
        [TestCase("cat and dog   ","tac dna god   ")] //triple

        public void Special_Words(string s, string expected)
        {
            RedPill redpill = new RedPill();
            var actual = redpill.ReverseWords(s);

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void ALL_SPACESES()
        {
            RedPill redpill = new RedPill();
            var expected = " A l l S p a c e s ";
            var actual = redpill.ReverseWords(" A l l S p a c e s ");

            Assert.AreEqual(expected, actual);
        }

        [TestCase("OddWord","droWddO")]
        [TestCase("evenWord", "droWneve")]
        [TestCase("O", "O")]
        [TestCase("eW", "We")]
        public void SingleWordReverse(string s,string expected) //odd
        {
            RedPill redpill = new RedPill();
            
            var actual = redpill.ReverseWords(s);

            Assert.AreEqual(expected, actual);
        }
       
        [Test]
        public void SingleSpace_shouldBe_SingleSpace()
        {
            RedPill redpill = new RedPill();
            var expected = " ";
            var actual = redpill.ReverseWords(" ");

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void NULL_should_ThrowExcption()
        {
            RedPill redpill = new RedPill();
            Assert.Throws<NullReferenceException>(delegate
            {
                redpill.ReverseWords(null);
            });
            
        }
        [Test]
        public void Empty_shouldbe_Empty()
        {
            RedPill redpill = new RedPill();
            var expected = string.Empty;
            var actual = redpill.ReverseWords(string.Empty);

            Assert.AreEqual(expected, actual);
        }
    }
}
