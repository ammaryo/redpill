﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;



namespace KnockKnock
{

    [ServiceBehavior(Namespace = "http://knockingreadify.apphb.com", ConfigurationName = "IRedPill", IncludeExceptionDetailInFaults = true)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RedPill : IRedPill
    {
        



        public RedPill()
        {
            //build fibonacci Series for fast call next time if not built before
            if (fibonanciiSeries.Count - 1 < MAX_FIB_NUMBER)
                FibonacciNumber(MAX_FIB_NUMBER);
        }

        #region WhoAreYou
        ContactDetails AmmarsContacats { get; set; }
        public ContactDetails WhoAreYou()
        {


            return ContactDetails.Ammar();


        } 
        #endregion

        public TriangleType WhatShapeIsThis(int a, int b, int c)
        {

            TriangleType result = TriangleType.Scalene;

            if (a <= 0 || b <= 0 || c <= 0)
                result = TriangleType.Error;
            else if ((long)a + (long)b <= (long)c || (long)a + (long)c <= (long)b || (long)b + (long)c <= (long)a) // cast int to long to accept int.max + int.max
                result = TriangleType.Error;

            else if (a == b && c == b)
                result = TriangleType.Equilateral;

            else if (a == b || c == a || b == c)
                result = TriangleType.Isosceles;

            return result;

        }


        #region FibonacciNumber
        const long MAX_FIB_NUMBER = 92;
        static List<long> fibonanciiSeries = new List<long>();
        public long FibonacciNumber(long n)
        {


            if (n > MAX_FIB_NUMBER || n < -1 * MAX_FIB_NUMBER)
            {

                throw new OverflowException(string.Format("Fib(>{0}) will cause a 64-bit integer overflow", MAX_FIB_NUMBER));

            }
            var sign = 1;
            if (n < 0 && n % 2 == 0) sign = -1; // result of negative even numbers should be negative http://en.wikipedia.org/wiki/Fibonacci_number#Negafibonacci
            n = Math.Abs(n);


            if (fibonanciiSeries.Count - 1 < n) // if not calculated before
            {

                long previousValue = fibonanciiSeries.Count == 0 ? -1 : fibonanciiSeries[(int)n - 2];

                long currentResult = fibonanciiSeries.Count == 0 ? 1 : fibonanciiSeries[(int)n - 1];

                for (int i = fibonanciiSeries.Count; i <= n; ++i)
                {
                    long sum = currentResult + previousValue;
                    previousValue = currentResult;
                    currentResult = sum;
                    fibonanciiSeries.Insert(i, sum);
                }
            }




            return fibonanciiSeries[(int)n] * sign;
        }
        
        #endregion



        #region ReverseWords
        public string ReverseWords(string s)
        {

            //cat and dog ==> tac dan god 
            // the alogrithim complex is O(1.5*N);
            if (s == null) throw new NullReferenceException("Value cannot be null");
            if (s.Length == 0) return "";

            char[] result = new char[s.Length];


            int i = 0; // first cursor
            while (i < s.Length)
            {

                while (i < s.Length && s[i] == ' ') result[i++] = ' '; // if word starts with spaces, put these spaces in the same position from begining till you find first char

                int j = i + 1; // init j cursor 


                while (j < s.Length && s[j] != ' ') j++; // find next space - end of the current word - by j cursor



                ReverseSingleWord(s, result, i, j - 1); // the end cursor (j) should start from last char, not from the space 


                i = j; // next i should start from next word

            }
            return new String(result);

        }


        private static void ReverseSingleWord(string s, char[] result, int i, int j)
        {
            //setp 1. even 
            //result= [n,,,e] i=0,j=3
            //step 2. even 
            //result = [n,e,v,e] i=1,j=2

            //setp 1. 1Word
            //result= [d,,,1] i=0,j=4
            //step 2. 1Word
            //result = [d,r,,W,1] i=1,j=3
            //step 3. 1Word
            //result = [d,r,o,w,1] i=2,j=2

            //swap word[i] with word[j] 
            //complex O(N/2)
            while (i < s.Length && j >= i) // for odd lenght word i will be equal to j 
            {
                result[i] = s[j];
                result[j] = s[i];
                //move i forward and j backword
                i++; j--;

            }


        }
        
        #endregion
    }
}
