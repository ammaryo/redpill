﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace KnockKnock
{
    
    [GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [DataContractAttribute(Name = "TriangleType", Namespace = "http://KnockKnock.readify.net")]
    public enum TriangleType 
    {
        [EnumMemberAttribute(Value="Equilateral")]
        Equilateral ,
        [EnumMemberAttribute(Value="Isosceles")]
         Isosceles ,
        [EnumMemberAttribute(Value="Error")]
        Error ,
        [EnumMemberAttribute(Value="Scalene")]
        Scalene 
    }
}
