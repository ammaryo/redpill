﻿using KnockKnock;
using System.CodeDom.Compiler;
using System.ServiceModel;
using System.Threading.Tasks;

[GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
[ServiceContractAttribute(Namespace = "http://KnockKnock.readify.net", ConfigurationName = "IRedPill")]
public interface IRedPill
{
    [OperationContractAttribute(Action = "http://KnockKnock.readify.net/IRedPill/WhoAreYou",
                                ReplyAction = "http://KnockKnock.readify.net/IRedPill/WhoAreYouResponse")]
    ContactDetails WhoAreYou();

    [OperationContractAttribute(Action = "http://KnockKnock.readify.net/IRedPill/FibonacciNumber",
                                ReplyAction = "http://KnockKnock.readify.net/IRedPill/FibonacciNumberResponse")]
    [FaultContractAttribute(typeof(System.ArgumentOutOfRangeException),
                            Action = "http://KnockKnock.readify.net/IRedPill/FibonacciNumberArgumentOutOfRangeExceptionFault",
                            Name = "ArgumentOutOfRangeException",
                            Namespace = "http://schemas.datacontract.org/2004/07/System")]
    long FibonacciNumber(long n);

    [OperationContractAttribute(Action = "http://KnockKnock.readify.net/IRedPill/WhatShapeIsThis",
                                ReplyAction = "http://KnockKnock.readify.net/IRedPill/WhatShapeIsThisResponse")]
    TriangleType WhatShapeIsThis(int a, int b, int c);

    [OperationContractAttribute(Action = "http://KnockKnock.readify.net/IRedPill/ReverseWords",
                                ReplyAction = "http://KnockKnock.readify.net/IRedPill/ReverseWordsResponse")]
    [FaultContractAttribute(typeof(System.ArgumentNullException),
                            Action = "http://KnockKnock.readify.net/IRedPill/ReverseWordsArgumentNullException" + "Fault",
                            Name = "ArgumentNullException", Namespace = "http://schemas.datacontract.org/2004/07/System")]
    string ReverseWords(string s);
}