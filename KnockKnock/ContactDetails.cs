﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace KnockKnock
{
    [DebuggerStepThroughAttribute()]
    [GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [DataContractAttribute(Name = "ContactDetails", Namespace = "http://KnockKnock.readify.net")]
    public partial class ContactDetails : Object //IExtensibleDataObject
    {
        [DataMemberAttribute()]
        public string EmailAddress { get; set; }

        [DataMemberAttribute()]
        public string FamilyName { get; set; }

        [DataMemberAttribute()]
        public string GivenName { get; set; }

        [DataMemberAttribute()]
        public string PhoneNumber { get; set; }


        public override string ToString()
        {
            return EmailAddress + FamilyName + GivenName + PhoneNumber;
        }
        private static ContactDetails ammar;
        
        public static ContactDetails Ammar()
        {
            if (ammar == null)
            {
                ammar = new ContactDetails
                {
                    EmailAddress = "3mmar.osama@gmail.com",
                    GivenName = "Ammar",
                    FamilyName = "Osama",
                    PhoneNumber = "+201206502995"
                };
            }

            return ammar;
        }

      
    }
}
